
`npm run dev` - Runs `npm run dev` to start Next.js in development mode  
`npm run build` - Runs `npm run build` to build the application for production  usage  
`npm run start` - Runs `npm run start` to start a Next.js production server  
`npm run lint` - Runs `npm run lint` to set up Next.js' built-in ESLint configuration
