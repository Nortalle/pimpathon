import dynamic from 'next/dynamic';

const Sketch = dynamic(
	() => import('../components/sketch').then((mod) => mod.Sketch),
	{
		ssr: false,
	},
);


export default function App() {
    return <Sketch />
  }